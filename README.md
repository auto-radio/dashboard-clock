# Dashboard Clock

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-dashboard.png" width="80" align="right" />

Dashboard Clock is a web application for displaying a studio clock which is part of
[aura.radio](https://aura.radio).

## Prerequisites

Before you begin, ensure you have met the following requirements:

- [make](https://www.gnu.org/software/make/)
- [Node.js](https://nodejs.org/)

## Configuring Dashboard Clock

Following command installs all dependencies and creates a `.env` file in the root of the project

```bash
make init.app
```

This creates a `.env` file in the root of the project. Modify its values matching your needs.

## Using Dashboard Clock

### Build the bundle for deployment to web server

In order to apply configuration changes a build is required.

```bash
make build
```

Copy the bundle files in `public/build/` to some web server accessible by your studio computers.

### Run with included `sirv` server

Alternatively, you can start the clock with the production-ready [sirv](https://github.com/lukeed/sirv/tree/master/packages/sirv) server

```bash
make start
```

Open [localhost:5000](http://localhost:5000) to see the clock.

This server is not doing any hot-reloading but building the bundle on startup. So any changes in the `.env` file
will be included.

### Run using Docker

```py
# to be implemented and documented (see #19 and #20).
```

## Developing Dashboard Clock

Install the NPM requirements

```bash
make init.dev
```

Then start the development server

```bash
make run
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`,
save it, and reload the page to see your changes.

By default, the server will only respond to requests from localhost. To allow connections from other computers,
edit the `sirv` commands in package.json to include the option `--host 0.0.0.0`.

## About

[<img src="https://aura.radio/assets/aura-logo-grayscale.png" width="150" />](https://aura.radio)

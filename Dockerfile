FROM node:lts-alpine AS base
LABEL maintainer="David Trattnig <david.trattnig@subsquare.at>"

# Base Stage

WORKDIR /aura
COPY package*.json ./
RUN npm install -g npm@latest
RUN npm install
RUN apk add bash make
EXPOSE 5000
ENV HOST=0.0.0.0
COPY . .

# Development Stage

FROM base AS dev
CMD ["make", "run"]

# Production Stage

FROM base AS prod
CMD ["make", "start"]

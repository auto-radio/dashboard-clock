# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- ...

### Changed

- ...

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

### Security

- ...

## [1.0.0-alpha2] - 2023-06-19

### Changed

- Provide properties in API schemas in CamelCase notation (aura#141)

## [1.0.0-alpha1] - 2023-02-27

Initial release.


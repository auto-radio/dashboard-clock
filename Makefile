-include build/base.Makefile
-include build/docker.Makefile

help::
	@echo "$(APP_NAME) targets:"
	@echo "    init.app        - init application environment"
	@echo "    init.dev        - init development environment"
	@echo "    lint            - verify code style"
	@echo "    spell           - check spelling of text"
	@echo "    clean           - clean artifacts"
	@echo "    build           - build a production bundle"
	@echo "    dist            - create a dist package"
	@echo "    run             - start app in development mode"
	@echo "    start           - start app with sirv server (public)"
	@echo "    release         - tag and push release with current version"
	$(call docker_help)


# Settings

TIMEZONE := "Europe/Vienna"

DOCKER_TARGET = "prod"
DOCKER_RUN = @docker run \
		--name $(APP_NAME) \
		--network="host" \
		-e TZ=$(TIMEZONE) \
		-u $(UID):$(GID) \
		$(DOCKER_ENTRY_POINT) \
		autoradio/$(APP_NAME)


# Targets

init.app:: package.json
	npm install
	cp config/default-sample.env .env

init.dev:: init.app
	npm install -g cspell@latest

lint::
	npx svelte-check

spell::
	npm run spell

clean::
	rm -rf dist
	mkdir -p dist/assets

build::
	npm run build

dist::clean
dist::build
dist::
	cp public/build/aura-clock-bundle.* dist
	cp index.html

run::
	npm run dev

start::
	npm run build
	npm run start --host

release:: VERSION := $(shell npm pkg get version | sed 's/"//g')
release::
	git tag $(VERSION)
	git push origin $(VERSION)
	@echo "Release '$(VERSION)' tagged and pushed successfully."
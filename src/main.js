import StudioClock from './StudioClock.svelte';

var app = new StudioClock({
    target: document.body
});

export default app;